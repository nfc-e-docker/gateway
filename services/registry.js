'use strict';

const registryStore = new Map();

exports.registry = ({ name, ipv4, port }) => (registryStore.set(name, { ipv4, port }) ? true : false);

exports.unregister = name => registryStore.delete(name);

exports.find = name => registryStore.get(name);
