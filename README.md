# Gateway

O serviço de gateway tem como principais atribuições o roteamento de requisições, autenticação de clientes, promoção da interoperabilidade de acordo com a representação de dados adotada pelo cliente e validação das requisições.

Abaixo, uma visão macro da arquitetura proposta:

![alt text](./overview.jpg 'Visão geral da arquitetura')

Em seguida, é apresentado o padrão de comunicação utilizado pelos microserviços:

![alt text](./communication.jpg 'Padrão de comunicação entre os serviços')

**Execução do serviço**

Testes de unidade: `npm run test`

Inicialização: `npm start`

**_A utilização do serviço exige que uma instância do MongoDB esteja disponível. A configuração deve ser feita por meio das variáveis de ambiente disponíveis na seção `# Mongo DB` no arquivo .env_**
