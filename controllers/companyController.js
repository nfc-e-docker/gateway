'use strict';

const { mongo } = require('../lib');
const { promiseHandler, crypto } = require('../util');

let data = null;
let err = null;

// Company schema.
const createCompany = company => {
  const { cnpj, cert, email, ie, im, name, phone, address, certPassword, platformPassword, csc, cic } = company;
  return {
    name,
    cnpj,
    cert,
    email,
    ie,
    im,
    phone,
    address,
    certPassword,
    platformPassword,
    cic,
    csc
  };
};

// Lists all registred companies.
exports.getAll = (req, res) => {
  res.status(200).send('Clients were sent');
};
// Sends a specific company according its CNPJ.
exports.getOne = async (req, res) => {
  [err, data] = await promiseHandler(mongo.findCompany(req.params['cnpj']));
  if (err) return res.status(500).send(err);
  res.status(200).send(data);
};
// Saves a company in the platform.
exports.insert = async (req, res) => {
  if (typeof req.body.cert.pub === 'string' && typeof req.body.cert.key === 'string') {
    req.body.cert.crt = Buffer.from(req.body.cert.pub, 'base64');
    req.body.cert.key = Buffer.from(req.body.cert.key, 'base64');
  }
  const company = createCompany(req.body);

  // Password hashing
  company.platformPassword = crypto(company.platformPassword, company.cnpj);
  company.certPassword = crypto(company.certPassword, company.cnpj);

  [err, data] = await promiseHandler(mongo.insertCompany(company));

  if (err) return res.status(500).send(err);
  res.status(201).send({ insertedCount: data.insertedCount });
};
// Updates company info.
exports.update = async (req, res) => {
  [err, data] = await promiseHandler(mongo.findCompany(req.body.cnpj));
  if (err || !data) res.status(err ? 500 : 404).send(err ? 'Internal server error' : 'Not found');

  if (typeof req.body.cert.pub === 'string' && typeof req.body.cert.key === 'string') {
    req.body.cert.crt = Buffer.from(req.body.cert.pub, 'base64');
    req.body.cert.key = Buffer.from(req.body.cert.key, 'base64');
  }
  const company = createCompany(req.body);
  // Password hashing
  company.platformPassword = crypto(company.platformPassword, company.cnpj);
  company.certPassword = crypto(company.certPassword, company.cnpj);

  let obj = {};
  obj = Object.assign(obj, company);
  delete obj.cnpj;

  [err, data] = await promiseHandler(mongo.updateCompany(company.cnpj, obj));
  if (err) return res.status(500).send(err);

  res.status(200).send(data.result);
};
// Removes a company (if a company is removed, your access will be blocked).
exports.remove = async (req, res) => {
  [err, data] = await promiseHandler(mongo.removeCompany(req.params['cnpj']));
  if (err) return res.status(500).send(err);
  res.status(200).send(data.result);
};
