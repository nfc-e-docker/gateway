'use strict';

const { mongo } = require('../lib');
const { promiseHandler } = require('../util');

let data = null;
let err = null;

exports.getLog = async (req, res) => {
  const { start, end, limit } = req.query;

  [err, data] = await promiseHandler(mongo.findLog(start, end, limit));
  if (err) return res.status(500).send('Could not get log');
  res.status(200).send(data);
};
