'use strict';

const { mongo } = require('../lib');
const createClient = require('../lib/client');
const { find } = require('../services/registry');
const { promiseHandler, ufCode } = require('../util');
const { Builder } = require('xml2js');

let err = null;
let data = null;

exports.echo = async (req, res) => {
  const { uf } = req.params;
  const service = find(uf);
  if (!service) return res.status(503).send(`${uf} service is unavailable`);
  const client = await createClient(service.ipv4, service.port, 'InvoiceService');

  client.echo({ ping: 'ping' }, (err, reply) => {
    if (err) return res.status(503).send(`${uf} service is unavailable`);
    res.status(200).send(reply);
  });
};

exports.process = async (req, res) => {
  const uf = req.body.enviNFe.NFe.infNFe.ide.cUF;
  const cnpj = req.body.enviNFe.NFe.infNFe.emit.CNPJ;
  [err, data] = await promiseHandler(mongo.findCompany(cnpj));

  if (err || !data) return res.status(!data ? 404 : 500).send(err || 'company not found');
  // const company = data.doc
  const company = data;
  if (company.address.uf !== ufCode[uf])
    return res.status(405).send('Invoice UF code must be the same used by the company');

  const service = find(ufCode[uf]);

  if (!service) return res.status(503).send(`${uf} service is unavailable`);

  const client = await createClient(service.ipv4, service.port, 'InvoiceService');
  // company.cert.key.data
  client.process(
    { invoice: JSON.stringify(req.body), key: company.cert.key.buffer, crt: company.cert.crt.buffer },
    (err, reply) => {
      if (err) return res.status(500).send(err);
      const obj = JSON.parse(reply.invoice);
      res.status(200).send(obj);
    }
  );
};

exports.cancel = async (req, res) => {
  const key = req.params.key;
  // The first two numbers represent the UF.
  const uf = ufCode[key.substring(0, 2)];
  const service = find(uf);
  if (!service) return res.status(503).send(`${uf} service is unavailable`);
  const client = await createClient(service.ipv4, service.port, 'InvoiceService');

  client.cancel({ key }, (err, reply) => {
    if (err) return res.status(500).send(err);
    res.status(200).send(reply);
  });
};

exports.status = async (req, res) => {
  const uf = req.params.uf;
  const service = find(uf);
  if (!service) return res.status(503).send(`${uf} service is unavailable`);
  const client = await createClient(service.ipv4, service.port, 'InvoiceService');
  client.status({ uf }, (err, reply) => {
    if (err) return res.status(500).send(err);
    return res.status(200).send(reply);
  });
};

exports.documentFromXml = async (req, res) => {
  const width = parseInt(req.params.width || 210);
  const uf = req.body.nfeProc.NFe.infNFe.ide.cUF;
  const cnpj = req.body.nfeProc.NFe.infNFe.emit.CNPJ;

  [err, data] = await promiseHandler(mongo.findCompany(cnpj));
  if (err || !data) return res.status(!data ? 404 : 500).send(err || 'company not found');

  const company = data;
  if (company.address.uf !== ufCode[uf])
    return res.status(405).send('Invoice UF code must be the same used by the company');

  const service = find(ufCode[uf]);
  if (!service) return res.status(503).send(`${uf} service is unavailable`);
  [err, data] = await promiseHandler(createClient(service.ipv4, service.port, 'InvoiceService'));
  if (err) res.status(500).send(err);

  const client = data;
  // Transforms the object to xml representation and sends its content to DANFE service.
  const builder = new Builder({ headless: true, explicitRoot: false, renderOpts: { pretty: false } });
  const obj = builder.buildObject(req.body);

  client.document({ invoice: obj, cic: company.cic, csc: company.csc, width }, (err, reply) => {
    if (err) return res.status(500).send(err);
    res.status(200).send(reply.pdf);
  });
};
