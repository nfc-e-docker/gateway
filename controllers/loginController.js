'use strict';

const jwt = require('jsonwebtoken');
const { crypto } = require('../util');
const { mongo, certs } = require('../lib');

exports.login = async (req, res) => {
  try {
    const { cnpj, platformPassword } = req.body;

    const company = await mongo.findCompany(cnpj);
    if (!company) return res.status(401).send('Unauthorized');

    if (!company) return res.status(401).send('Unauthorized');

    if (crypto(platformPassword, company.cnpj) === company.platformPassword) {
      // Create a new toke with the company CNPJ document in the payload.
      // SHA 512 is used as a set of cryptographic hash functions.
      jwt.sign(
        { cnpj: company.cnpj },
        certs.keyCertPairs[0].private_key,
        { algorithm: 'HS512', expiresIn: '72h' },
        (err, token) => {
          if (err) return res.status(500).send(err);
          return res.status(200).send(token);
        }
      );
    } else {
      res.status(401).send('Unauthorized');
    }
  } catch (err) {
    res.status(500).send(err);
  }
};
