'use strict';

const path = require('path');
const grpc = require('grpc');
const https = require('https');
const express = require('express');
const { mongo, certs } = require('./lib');
const protoLoader = require('@grpc/proto-loader');
const { registry, find, unregister } = require('./services/registry');

const app = express();
// Internally, the gateway always will work with JSON format.
app.use(express.json());

// Http routes allow the clients to access all platform services. It's exposed to the world.
app.use('/', require('./routes'));

module.exports.init = () => {
  return new Promise(async (resolve, reject) => {
    // Opens mongo connection
    await mongo.openConnection();
    if (!mongo) reject();
    const server = new grpc.Server();

    // Proto loading
    const packageDefinition = await protoLoader.load(path.join(__dirname, 'protos', 'gateway.proto'));
    if (!packageDefinition) reject();

    const RegistryService = grpc.loadPackageDefinition(packageDefinition).RegistryService;

    // Available registry services
    server.addService(RegistryService.service, {
      register: (call, callback) => {
        const res = registry(call.request);
        if (!res) return callback({ message: 'Unable to register service' });
        // eslint-disable-next-line no-console
        console.info(`\t${call.request.name} was registred under ${call.request.ipv4}:${call.request.port}`);
        callback(null, { message: 'Registred' });
      },
      unregister: (call, callback) => {
        const res = unregister(call.request.name);
        if (!res) return callback({ message: 'Unable to unregister service' });
        // eslint-disable-next-line no-console
        console.info(`\t${call.request.name} was unregistred`);
        callback(null, { message: 'Unregistered' });
      },
      discover: (call, callback) => {
        const res = find(call.request.name);
        if (!res) return callback({ message: 'Service not found' });
        // eslint-disable-next-line no-console
        console.info(`\t${call.request.name} was discovered under ${res.ipv4}:${res.port}`);
        callback(null, res);
      },
      echo: (call, callback) => {
        callback(null, { pong: 'pong' });
      }
    });

    server.bind(
      `${process.env.GRPC_HOST}:${process.env.GRPC_PORT}`,
      grpc.ServerCredentials.createSsl(certs.rootCerts, certs.keyCertPairs, true)
    );
    server.start();

    https
      .createServer(
        {
          key: certs.keyCertPairs[0].private_key,
          cert: certs.keyCertPairs[0].cert_chain,
          ca: certs.rootCerts
        },
        app
      )
      .listen(process.env.HTTP_PORT, () => {
        resolve();
      });
  });
};
