## Orientações Gerais

## Requisitos para execução local:

- Node JS instalado na máquina;
- Container MongoDB disponível (especificar a url de conexão no arquivo `.env` por meio da variável de ambiente `MONGO_URL` em cada projeto;
- Executar a instalação de dependências em cada um dos módulos por meio do comando `npm install`.

Scripts com extensão `sh` e `bat` são destinados aos ambientes Linux e Windows respectivamente.

## Descrição dos scripts:

- **integrated_test**: Inicia todos os serviços e executa os testes de forma integrada;

- **normal_test**: Inicia todos os serviços e executa testes de unidade em cada um deles;
- **start**: Inicia todos os serviços para operação de forma integrada.

A execução dos testes também pode ser feita manualmente:

- geração de documentação (projeto Gateway): `npm run doc`;
- análise de qualidade de código com ESLint: `npm run lint`;
- testes de unidade: `npm run test`.

**Observações:**

- Ambas as requisições apresentadas abaixo operam com representação de dados em JSON ou XML;
- Caso teste a aplicação utilizando ferramentas externas como o Postman, Browser, HttpRequester, entre outras, lembre-se sempre de fornecer o token de acesso obtido na etapa de login no cabeçalho de cada requisição. Um exemplo de como se fazer isso via Postman pode ser conferido <a href="https://learning.getpostman.com/docs/postman/sending_api_requests/authorization/">aqui</a>.
