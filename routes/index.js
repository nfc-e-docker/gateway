'use strict';

const router = require('express').Router();
const xmlparser = require('express-xml-bodyparser');
const { checkAuth, notFound, dataTransform } = require('../middleware');

/**
 * @description Middlewares
 */

// Content-type interoperability.
router.use(dataTransform);
// Allows express to understand XML content and puts it in "req.body" section.
router.use(xmlparser({ explicitArray: false, explicitRoot: false, normalizeTags: false, attrkey: 'attrkey' }));

// Only login route get out from authentication check.
// Users may use it to get a valid token.

router.use('/login', require('./loginRoute'));

// Authentication check.
router.use(checkAuth);

router.use('/log', require('./logRoute'));

router.use('/nfce', require('./nfceRoute'));

router.use('/company', require('./companyRoute'));

// No route match. It gonna trigger "not found" handler.
router.use(notFound);

router.use((err, req, res) => {
  res.status(err.status).send('Not found');
});

module.exports = router;
