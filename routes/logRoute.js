'use strict';

const router = require('express').Router();
const { logValidator } = require('../validators');
const LogController = require('../controllers/logController');

/**
 * @api {get} /log Retorno de logs
 * @apiVersion 1.0.0
 * @apiGroup Log
 * @apiDescription Fornece o histórico de atividades executadas dentro da plataforma.
 * @apiSuccess {Array} array Conjunto de registros para o período especificado.
 * @apiErrorExample Resposta de erro:
 * HTTP/1.1 500 Internal Server Error
 * {
 *      Internal Server Error
 * }
 * @apiSuccessExample Resposta de sucesso:
 *     HTTP/1.1 200 OK
 *     {
 *       [ { _id: '5d602bc717ff1015fc8683aa',
 *          url: '/company/25906199000142',
 *           date: '2019-08-23T18:09:11.481Z' },
 *         { _id: '5d602bc717ff1015fc8683ab',
 *          url: '/company',
 *           date: '2019-08-23T18:09:11.786Z' },
 *        }]
 *     }
 * @apiParam {Date} start Período inicial.
 * @apiParam {Date} end Período final.
 * @apiParam {Number} limit Quantidade de registros a serem retornados.
 * @apiExample Exemplo:
 * https://localhost:3019/log?start=2019-08-23T18:09:11.786Z&end=2019-08-23T18:09:11.786Z&limit=10
 */
router.get('/', logValidator.get, LogController.getLog);

module.exports = router;
