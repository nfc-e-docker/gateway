'use strict';

const router = require('express').Router();
const LoginController = require('../controllers/loginController');

/**
 * @api {post} /login Login de empresa
 * @apiVersion 1.0.0
 * @apiGroup Login
 * @apiDescription Realiza o login de uma empresa dentro da plataforma.
 * @apiSuccess {String} token Token de acesso válido pelas próximas 72 hrs.
 * @apiErrorExample Resposta de erro (empresa não cadastrada):
 * HTTP/1.1 404 Not Found
 * {
 *      Company not found
 * }
 * @apiErrorExample Resposta de erro :
 * HTTP/1.1 500 Internal Server Error
 * {
 *      Internal Server Error
 * }
 * @apiSuccessExample Resposta de sucesso:
 *     HTTP/1.1 200 OK
 *     {
 *       eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJjbnBqIjoiMjU5MDYxOTkwMDAxNDIiLCJpYXQiOjE1NjcwODU4NzksImV4cCI6MTU2NzM0NTA3OX0.pbJ6J1C47r8yS6VD_SiiU_yJpDSoqoZqXcnyXOSwTbGqGP_yR8UImREmpkp9FDjbIpqosqKJka_wrVYnWP4Q_A
 *     }
 * @apiParam {String} cnpj CNPJ da empresa.
 * @apiParam {String} platformPassword Senha de acesso à plataforma.
 * @apiExample Exemplo:
 * https://localhost:3019/login
 */
router.post('/', LoginController.login);

module.exports = router;
