'use strict';

const router = require('express').Router();
const NfceController = require('../controllers/nfceController');

/**
 * @api {get} /nfce/echo/:uf Status de microsserviço
 * @apiVersion 1.0.0
 * @apiGroup Nfce
 * @apiDescription Permite checar se determinado microsserviço de UF está ativo ou não.
 * @apiSuccess {Object} pong
 * @apiErrorExample Resposta de erro:
 * HTTP/1.1 503 Unavailable Service
 * {
 *      unavailable service
 * }
 * @apiSuccessExample Resposta de sucesso:
 *     HTTP/1.1 200 OK
 *     {
 *       "ping": "pong"
 *     }
 * @apiParam {String} uf UF de consulta.
 * @apiExample Exemplo:
 * https://localhost:3019/nfce/echo/mg
 */
router.get('/echo/:uf', NfceController.echo);

/**
 * @api {post} /nfce/process Autorização de nota
 * @apiVersion 1.0.0
 * @apiGroup Nfce
 * @apiDescription Envia uma nota para autorização.
 * @apiSuccess {Object} body Nota assinada e autorizada com a tag "protNFe" inserida.
 * @apiErrorExample Resposta de erro:
 * HTTP/1.1 500 Internal Server Error
 * {
 *      Internal Server Error
 * }
 * @apiSuccessExample Resposta de sucesso:
 *     HTTP/1.1 200 OK
 *     {
 *       "enviNFe": {"NFe": "Campos internos da NFC-e", "protNFe": "Campos internos de autorização" }
 *     }
 * @apiParam {Object} enviNFe conteúdo do XML da nota.
 * @apiExample Exemplo:
 * https://localhost:3019/nfce/process
 */
router.post('/process', NfceController.process);

/**
 * @api {get} /nfce/status/uf Status de UF
 * @apiVersion 1.0.0
 * @apiGroup Nfce
 * @apiDescription Permite checar o estado operante/não operante de uma determinada UF.
 * @apiSuccess {String} pong
 * @apiErrorExample Resposta de erro:
 * HTTP/1.1 503 Unavailable Service
 * {
 * unavailable service
 * }
 * @apiSuccessExample Resposta de sucesso:
 *     HTTP/1.1 200 OK
 *     {
 *       "ping": "pong"
 *     }
 * @apiParam {String} uf UF de consulta.
 * @apiExample Exemplo:
 * https://localhost:3019/echo/mg
 */
router.get('/status/:uf', NfceController.status);

/**
 * @api {post} /nfce/document/:width Geração de documento DANFE
 * @apiVersion 1.0.0
 * @apiGroup Nfce
 * @apiDescription Geração de documento de DANFE a partir de um xml de nota autorizado.
 * @apiSuccess {File} pdf PDF do documento de DANFE.
 * @apiErrorExample Resposta de erro:
 * HTTP/1.1 500 Internal Server Error
 * {
 *      Internal Server Error
 * }
 * @apiSuccessExample Resposta de sucesso:
 *     HTTP/1.1 200 OK
 *     {
 *       "pdf": "Conteúdo binário do arquivo PDF"
 *     }
 * @apiParam {Number} width Largura do papel que será utilizado para impressão do DANFE (48, 80 ou 210 mm).
 * @apiParam {Object} nfeProc Nota fiscal já assinada e autorizada. Pode ser enviada em formato XML ou JSON.
 * @apiExample Exemplo:
 * https://localhost:3019/nfce/document/80
 */
router.post('/document/:width', NfceController.documentFromXml);

/**
 * @api {delete} /nfce/cancel/:key Canecelamento de uma NFC-e
 * @apiVersion 1.0.0
 * @apiGroup Nfce
 * @apiDescription Cancelamento de uma nota autorizada que se encontra armazenada na plataforma.
 * @apiSuccess {File} pdf PDF do documento de DANFE.
 * @apiErrorExample Resposta de erro:
 * HTTP/1.1 500 Internal Server Error or Not Found
 * {
 *      "status": false
 * }
 * @apiSuccessExample Resposta de sucesso:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": true
 *     }
 * @apiParam {String} key Chave de 44 posições da NFC-e (mais detalhes aqui: https://www.oobj.com.br/bc/article/como-%C3%A9-formada-a-chave-de-acesso-de-uma-nf-e-nfc-e-de-um-ct-e-e-um-mdf-e-281.html).
 * @apiExample Exemplo:
 * https://localhost:3019/nfce/cancel/21190528139664000155550000000002511954797040
 */
router.delete('/cancel/:key', NfceController.cancel);

module.exports = router;
