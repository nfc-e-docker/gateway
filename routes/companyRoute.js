'use strict';

const router = require('express').Router();
const { companyValidator } = require('../validators');
const CompanyController = require('../controllers/companyController');

/**
 * @api {get} /company/:cnpj Obtém uma empresa
 * @apiVersion 1.0.0
 * @apiGroup Empresa
 * @apiDescription Obtém os dados de uma empresa cadastrada na plataforma.
 * @apiSuccess {Object} company Objeto que contém as informações cadastrais da empresa.
 * @apiErrorExample Resposta de erro (empresa não cadastrada):
 * HTTP/1.1 401 Not Found
 * {
 *      Unauthorized
 * }
 * @apiErrorExample Resposta de erro genérica:
 * HTTP/1.1 500 Internal Server Error
 * {
 *      Internal Server Error
 * }
 * @apiSuccessExample Resposta de sucesso:
 *     HTTP/1.1 200 OK
 *     {
 *       "company": "Informações da empresa selecionada"
 *     }
 * @apiParam {String} cnpj CNPJ da empresa.
 * @apiExample Exemplo:
 * https://localhost:3019/company/25906199000142
 */
router.get('/:cnpj', companyValidator.getOne, CompanyController.getOne);

/**
 * @api {post} /company Cadastra uma empresa
 * @apiVersion 1.0.0
 * @apiGroup Empresa
 * @apiDescription Cadastra uma empresa na plataforma. Esse método é utilizado apenas para fins de teste. Em um cenário real de uso, o cadastro deve ser feito diretamente pelo administrador do sistema na base Mongo.
 * @apiSuccess {Object} insertedCount Quantidade de registros inseridos. O valor esperado é que "insertedCount" seja sempre igual a um.
 * @apiErrorExample Resposta de erro genérica:
 * HTTP/1.1 500 Internal Server Error
 * {
 *      Internal Server Error
 * }
 * @apiSuccessExample Resposta de sucesso:
 *     HTTP/1.1 200 OK
 *     {
 *        "insertedCount": 1
 *     }
 * @apiParam {String} cnpj CNPJ da empresa.
 * @apiExample Exemplo:
 * https://localhost:3019/company
 */
router.post('/', companyValidator.insert, CompanyController.insert);

/**
 * @api {put} /company Utualiza uma empresa
 * @apiVersion 1.0.0
 * @apiGroup Empresa
 * @apiDescription Atualiza as informações de empresa na plataforma, salvo o valor de CNPJ.
 * @apiSuccess {Object} nModified Quantidade de registros modificados. O valor esperado é que "nModified" seja sempre igual a um.
 * @apiErrorExample Resposta de erro genérica:
 * HTTP/1.1 500 Internal Server Error
 * {
 *      Internal Server Error
 * }
 * @apiSuccessExample Resposta de sucesso:
 *     HTTP/1.1 200 OK
 *     {
 *       { "n": 1, "nModified": 1, "ok": 1 }
 *     }
 * @apiParam {Object} company Objeto com as informações a serem atualizadas. O envio do CNPJ é obrigatório - utilizado nas busca e validação de informações.
 * @apiExample Exemplo:
 * https://localhost:3019/company
 */
router.put('/', companyValidator.update, CompanyController.update);

/**
 * @api {delete} /company Remove uma empresa
 * @apiVersion 1.0.0
 * @apiGroup Empresa
 * @apiDescription Remove uma empresa da plataforma.
 * @apiSuccess {Object} n Quantidade de registros removidos. O valor esperado é que "n" seja sempre igual a um.
 * @apiErrorExample Resposta de erro genérica:
 * HTTP/1.1 500 Internal Server Error
 * {
 *      Internal Server Error
 * }
 * @apiSuccessExample Resposta de sucesso:
 *     HTTP/1.1 200 OK
 *     {
 *       { "n": 1, "ok": 1 }
 *     }
 * @apiParam {String} cnpj CNPJ da empresa.
 * @apiExample Exemplo:
 * https://localhost:3019/company
 */
router.delete('/:cnpj', companyValidator.remove, CompanyController.remove);

// router.get('/', CompanyController.getAll);
module.exports = router;
