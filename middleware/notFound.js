'use strict';

module.exports = (req, res, next) => {
  const err = { status: 404, url: req.url, message: 'Not found' };
  next(err);
};
