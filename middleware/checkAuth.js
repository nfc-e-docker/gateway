'use strict';

const jwt = require('jsonwebtoken');
const { mongo, certs } = require('../lib');

module.exports = (req, res, next) => {
  mongo.insertLog({ url: req.originalUrl, date: new Date() });

  try {
    let token = req.headers.authorization;
    if (!token) return res.status(401).send('Unauthorized');
    token = token.split(' ')[1];
    jwt.verify(token, certs.keyCertPairs[0].private_key, async (err, decoded) => {
      if (err) return res.status(500).send('Bad token content');

      const company = await mongo.findCompany(decoded.cnpj);
      if (company) next();
      else res.status(401).send('Unauthorized');
    });
  } catch (err) {
    res.status(500).send(err);
  }
};
