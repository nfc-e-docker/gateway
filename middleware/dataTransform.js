'use strict';

const { Builder } = require('xml2js');

module.exports = (req, res, next) => {
  if (req.originalUrl.endsWith('document')) {
    res.set('Content-Type', 'application/pdf');
  }
  if (req.headers['content-type'] === 'application/xml') {
    const oldSend = res.send;
    res.set('content-type', 'application/xml');
    res.send = function() {
      const builder = new Builder({
        renderOpts: { pretty: false },
        normalizeTags: false,
        headless: true,
        explicitRoot: false
      });
      arguments[0] = builder.buildObject(arguments[0]);
      oldSend.apply(res, arguments);
    };
  }
  next();
};
