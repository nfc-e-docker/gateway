'use strict';

module.exports = {
  checkAuth: require('./checkAuth'),
  notFound: require('./notFound'),
  dataTransform: require('./dataTransform')
};
