FROM node:10-alpine

LABEL maintainer "Rogério de Oliveira Batista"

ENV HOME=/usr/app/gateway

ENV NODE_ENV=production
# gRPC
ENV GRPC_PORT=3020
ENV GRPC_HOST=localhost

# HTTP
ENV HTTP_PORT=3019
ENV HTTP_HOST=localhost

# Mongo DB
ENV MONGO_URL=mongodb://localhost:27017
ENV MONGO_DB=nfce
ENV MONGO_COMPANY_COLLECTION=company
ENV MONGO_LOG_COLLECTION=log

WORKDIR $HOME
COPY ./ $HOME


RUN npm install --only=production

EXPOSE 3019
CMD [ "npm", "start" ]
