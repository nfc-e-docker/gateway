'use strict';

const mongo = require('mongodb').MongoClient;

let companyCollection = null;
let logColletion = null;
let db = null;

/**
 * @description Establishes the mongo connection.
 */
const openConnection = () => {
  return new Promise((resolve, reject) => {
    mongo
      .connect(process.env.MONGO_URL, { useNewUrlParser: true })
      .then(client => {
        db = client.db(process.env.MONGO_DB);
        companyCollection = db.collection(process.env.MONGO_COMPANY_COLLECTION);
        logColletion = db.collection(process.env.MONGO_LOG_COLLECTION);
        resolve();
      })
      .catch(err => {
        reject(err);
      });
  });
};

const insertLog = log => logColletion.insertOne(log);

/**
 * @description Gets a set of logs.
 * @param {Date} date Initial date.
 * @param {Date} end Final date.
 * @param {Number} limit Defines the maximum number of documents the cursor will return.
 * @returns {Array} Result set.
 */
const findLog = (start, end, limit) =>
  logColletion
    .find({ date: { $gte: new Date(start), $lt: new Date(end) } })
    .limit(parseInt(limit) || 10)
    .toArray();

const insertCompany = doc => companyCollection.insertOne(doc);

const findCompany = cnpj => companyCollection.findOne({ cnpj });

const updateCompany = (cnpj, doc) => companyCollection.updateMany({ cnpj }, { $set: { doc } });

const removeCompany = cnpj => companyCollection.deleteOne({ cnpj });

module.exports = { openConnection, insertCompany, findCompany, insertLog, findLog, updateCompany, removeCompany };
