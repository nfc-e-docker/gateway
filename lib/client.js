'use strict';

const grpc = require('grpc');
const path = require('path');
const { certs } = require('../lib');
const protoLoader = require('@grpc/proto-loader');

// Proto loading

/**
 * @description Creates a new grpc client according with ip (host) and port.
 */
module.exports = async (ipv4, port, service) => {
  const packageDefinition = await protoLoader.load(path.join(__dirname, '..', 'protos', 'gateway.proto'));
  const Service = grpc.loadPackageDefinition(packageDefinition)[service];
  return new Service(
    `${ipv4}:${port}`,
    grpc.credentials.createSsl(certs.rootCerts, certs.keyCertPairs[0].private_key, certs.keyCertPairs[0].cert_chain)
  );
};
