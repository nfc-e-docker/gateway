/* eslint-disable no-console */
'use strict';

if (process.env.NODE_ENV !== 'production') require('dotenv').config();
const server = require('./server');

server
  .init()
  .then(() => {
    console.info(`\tGrpc server running at ${process.env.GRPC_HOST}:${process.env.GRPC_PORT}`);
    console.info(`\tHttp server running at https://localhost:${process.env.HTTP_PORT}`);
  })
  .catch(() => {
    console.error('\tServer creation has been failed');
    process.exit(1);
  });
