'use strict';

const { validationResult, check } = require('express-validator');

/**
 * @description Company get validation.
 */
exports.getOne = [
  check('cnpj')
    .exists()
    .isLength({ min: 14, max: 14 }),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(422).json({ errors: errors.array() });
    next();
  }
];

/**
 * @description Company update validation.
 */
exports.update = [
  check(['name', 'email', 'phone', 'certPassword', 'platformPassword', 'ie', 'im', 'cic', 'csc']).isString(),
  check('address').exists(),
  check('cnpj').isLength(14),
  check('cert.key').custom(file => file.type === 'Buffer' || typeof file === 'string'),
  check('cert.crt').custom(file => file.type === 'Buffer' || typeof file === 'string'),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(422).json({ errors: errors.array() });
    next();
  }
];

/**
 * @description Company remotion validation.
 */
exports.remove = [];

/**
 * @description Company insertion validation.
 */
exports.insert = [
  check(['name', 'email', 'phone', 'certPassword', 'platformPassword', 'ie', 'im', 'cic', 'csc'])
    .optional()
    .isString(),
  check('address').optional(),
  check('cnpj').isLength(14),
  check('cert.key')
    .optional()
    .custom(file => file.type === 'Buffer' || typeof file === 'string'),
  check('cert.crt')
    .optional()
    .custom(file => file.type === 'Buffer' || typeof file === 'string'),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(422).json({ errors: errors.array() });
    next();
  }
];

/**
 * @description Company get validation.
 */
exports.getAll = [];
