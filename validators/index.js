'use strict';

module.exports = {
  companyValidator: require('./companyValidator'),
  invoiceValidator: require('./invoiceValidator'),
  logValidator: require('./logValidator')
};
