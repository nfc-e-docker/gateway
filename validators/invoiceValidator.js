'use strict';

/**
 * @description Invoice process validation.
 */
exports.process = [];

/**
 * @description Invoice cancel validation.
 */
exports.cancel = [];

/**
 * @description Invoice status validation.
 */
exports.status = [];

/**
 * @description Echo validation.
 */
exports.echo = [];
