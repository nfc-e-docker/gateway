'use strict';

const { validationResult, query } = require('express-validator');

/**
 * @description Get log validation.
 */
exports.get = [
  query('start').isISO8601(),
  query('end').isISO8601(),
  query('limit').isInt({ max: 10000 }),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    next();
  }
];
