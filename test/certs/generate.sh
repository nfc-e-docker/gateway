# openssl (pem from crt e key files)
openssl rsa -in server.key -text > private.pem
openssl x509 -inform PEM -in server.crt > public.pem

# crt and key files merge to pem

# Windows
type server.crt server.key > server.pem

# Linux
cat server.crt server.key > server.pem
