'use strict';

const fs = require('fs');
require('dotenv').config();
const path = require('path');
const request = require('request');
const { certs } = require('../lib');
const { should, expect } = require('chai');
const { parseString } = require('xml2js');
// const pfx = fs.readFileSync(`${__dirname}/certs/certificate.pfx`);

const options = {
  cert: certs.keyCertPairs[0].cert_chain,
  key: certs.keyCertPairs[0].private_key,
  ca: certs.rootCerts,
  json: true,
  body: {}
};
let key = null;
const url = `https://${process.env.HTTP_HOST}:${process.env.HTTP_PORT}`;
const unsigned = fs.readFileSync(path.join(__dirname, 'xml', 'mg', 'unsigned.xml'), { encoding: 'utf-8' });
const signed = fs.readFileSync(path.join(__dirname, 'xml', 'mg', 'signed.xml'), { encoding: 'utf-8' });

describe('Module: Invoice (MG)', () => {
  before(function(done) {
    expect(process.env.TOKEN.length).to.be.above(0);
    options.headers = { Authorization: `Bearer ${process.env.TOKEN}` };
    done();
  });

  it('Method: Echo (MG)', function(done) {
    request.get(`${url}/nfce/echo/mg`, options, (err, res) => {
      should().not.exist(err);
      expect(res.statusCode).to.be.oneOf([200, 503]);
      expect(res.body).to.satisfy(data => data === 'mg service is unavailable' || data.pong === 'pong');
      done();
    });
  });

  it('Method: Status (MG)', done => {
    options.body = {};
    request.get(`${url}/nfce/status/mg`, options, (err, res) => {
      should().not.exist(err);
      expect(res.body.status).to.be.a('boolean');
      done();
    });
  });

  it('Method: Send to process (MG)', done => {
    parseString(
      unsigned,
      { explicitArray: false, explicitRoot: true, normalizeTags: false, attrkey: 'attrkey' },
      (err, json) => {
        should().not.exist(err);
        options.body = json;
        request.post(`${url}/nfce/process`, options, (err, res) => {
          should().not.exist(err);
          should().exist(res.body.enviNFe.protNFe);
          key = res.body.enviNFe.NFe.infNFe.attrkey.Id.replace('NFe', '');
          done();
        });
      }
    );
  });

  it('Method: DANFE 210 mm (MG)', done => {
    parseString(
      signed,
      { explicitArray: false, explicitRoot: true, normalizeTags: false, attrkey: 'attrkey' },
      (err, json) => {
        options.body = json;
        options.encoding = null;
        should().not.exist(err);
        request.post(`${url}/nfce/document/210`, options, (err, res) => {
          should().not.exist(err);
          fs.writeFileSync(path.join(__dirname, 'out', '210mm.pdf'), res.body);
          done();
        });
      }
    );
  });
  it('Method: DANFE 80 mm (MG)', done => {
    parseString(
      signed,
      { explicitArray: false, explicitRoot: true, normalizeTags: false, attrkey: 'attrkey' },
      (err, json) => {
        options.body = json;
        options.encoding = null;
        should().not.exist(err);
        request.post(`${url}/nfce/document/80`, options, (err, res) => {
          should().not.exist(err);
          fs.writeFileSync(path.join(__dirname, 'out', '80mm.pdf'), res.body);
          done();
        });
      }
    );
  });

  it('Method: Cancel (MG)', done => {
    options.body = {};
    request.delete(`${url}/nfce/cancel/${key}`, options, (err, res) => {
      should().not.exist(err);
      expect(res.body.status).to.be.equal(true);
      done();
    });
  });
});
