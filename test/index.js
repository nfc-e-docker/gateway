'use strict';

const server = require('../server');
const { certs } = require('../lib');

describe('Module: Gateway', () => {
  before(done => {
    // Starts the gateway
    server
      .init(certs)
      .then(() => {
        done();
      })
      .catch(err => {
        done(err);
      });
  });

  if (process.env.TEST_TYPE === 'integrated') {
    it('Method: Wait for services', function(done) {
      // this.timeout(process.env.WAIT_FOR_SERVICES + 1000);
      // eslint-disable-next-line no-console
      console.info('\tWaiting for services wake up...');
      const time = setTimeout(() => {
        clearTimeout(time);
        // eslint-disable-next-line no-console
        console.info('\x1b[32m', '\tStarting integrated tests...');
        require('./mgInvoice');
        require('./baInvoice');
        done();
      }, process.env.WAIT_FOR_SERVICES);
    });
  }

  // Dicover tests.
  require('./discover');
  // Login tests.
  require('./login');
  // Company tests.
  require('./company');
  // Log tests.
  require('./log');
});
