'use strict';

const { should, expect } = require('chai');
const createClient = require('../lib/client');

let client = null;

describe('Module: Discover', () => {
  before(done => {
    createClient(process.env.GRPC_HOST, process.env.GRPC_PORT, 'RegistryService')
      .then(_client => {
        client = _client;
        done();
      })
      .catch(err => {
        done(err);
      });
  });
  it('Method: Register (AM)', done => {
    client.register({ name: 'am', ipv4: '127.8.9.0', port: '3560' }, (err, res) => {
      should().not.exist(err);
      expect(res.message).to.be.equal('Registred');
      done();
    });
  });

  it('Method: Discover (AM)', done => {
    client.discover({ name: 'am' }, (err, res) => {
      should().not.exist(err);
      expect(res.ipv4).to.be.equal('127.8.9.0');
      expect(res.port).to.be.equal('3560');
      done();
    });
  });

  it('Method: Unregister (AM)', done => {
    client.unregister({ name: 'am' }, (err, res) => {
      should().not.exist(err);
      expect(res.message).to.be.equal('Unregistered');
      done();
    });
  });
});
