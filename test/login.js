'use strict';

require('dotenv').config();
const path = require('path');
const request = require('request');
const { should, expect } = require('chai');
const { mongo, certs } = require('../lib');
const { readFileSync } = require('fs');
const { crypto } = require('../util');

const url = `https://localhost:${process.env.HTTP_PORT}`;

const key = readFileSync(path.join(__dirname, 'certs', 'private.key'));
const crt = readFileSync(path.join(__dirname, 'certs', 'public.crt'));

// Passwords must be stored as hashing.
const companies = [
  {
    name: 'Rc Bike Shop LTDA',
    cnpj: '25906199000142',
    email: 'catalent-sac@roche.com.br',
    ie: '125331380',
    im: '125331382',
    phone: '(31)97513-8751',
    address: { uf: 'mg', city: 'Montes Claros', neighborhood: 'Planalto', street: 'Avenida Bernardo Sayao, 774' },
    csc: '000001',
    cic: '000183',
    certPassword: crypto(process.env.PEM_PASSWORD, '25906199000142'),
    platformPassword: crypto(process.env.PLATFORM_PASSWORD, '25906199000142'),
    cert: { key, crt }
  },
  {
    name: 'Operadora E Agencia De Viagens Cvc Tur Ltda',
    cnpj: '44191666000140',
    email: 'fale_conosco@cvc.com.br',
    ie: '125331380',
    im: '125331382',
    phone: '(71)3082-6249',
    address: { uf: 'ba', city: 'Salvador', neighborhood: 'Caminho das Árvores', street: 'Av Tancredo Neves, 2915' },
    csc: '0002301',
    cic: '000103',
    certPassword: crypto(process.env.PEM_PASSWORD, '44191666000140'),
    platformPassword: crypto(process.env.PLATFORM_PASSWORD, '44191666000140'),
    cert: { key, crt }
  }
];

// As default, MG is used to login
const options = {
  cert: certs.keyCertPairs[0].cert_chain,
  key: certs.keyCertPairs[0].private_key,
  ca: certs.rootCerts,
  json: true,
  body: {
    cnpj: companies[0].cnpj,
    platformPassword: process.env.PLATFORM_PASSWORD
  }
};

describe('Module: Login', () => {
  before(done => {
    mongo
      .openConnection()
      .then(() => {
        done();
      })
      .catch(err => {
        should().not.exist(err);
        done();
      });
  });
  it('Method: insert company and login', done => {
    // First of all, we need to insert all companies.
    const promises = [];
    companies.forEach(company => {
      promises.push(mongo.insertCompany(company));
    });

    Promise.all(promises)
      .then(() => {
        request.post(`${url}/login`, options, (err, res) => {
          should().not.exist(err);
          expect(res.body).to.be.a('string');
          expect(res.body).to.not.equal('Unauthorized');
          process.env.TOKEN = res.body;
          done();
        });
      })
      .catch(err => {
        done(err);
      });
  });
});
