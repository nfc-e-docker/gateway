'use strict';

require('dotenv').config();
const request = require('request');
const { should, expect } = require('chai');
const { certs } = require('../lib');

const options = {
  cert: certs.keyCertPairs[0].cert_chain,
  key: certs.keyCertPairs[0].private_key,
  ca: certs.rootCerts,
  json: true
};

// It gonna get logs starting from 10 days ago.
const start = new Date();
start.setDate(start.getDate() - 10);
const end = new Date();
const url = `https://localhost:${process.env.HTTP_PORT}`;

describe('Module: Log', () => {
  before(done => {
    expect(process.env.TOKEN.length).to.be.above(0);
    options.headers = { Authorization: `Bearer ${process.env.TOKEN}` };
    done();
  });
  it('Method: getLog', done => {
    request.get(`${url}/log?start=${start.toISOString()}&end=${end.toISOString()}&limit=10`, options, (err, res) => {
      should().not.exist(err);
      should().exist(res);
      expect(res.body).to.have.lengthOf.at.least(0);
      done();
    });
  });
});
