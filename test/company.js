'use strict';

const fs = require('fs');
require('dotenv').config();
const path = require('path');
const request = require('request');
const { certs } = require('../lib');
const { should, expect } = require('chai');
const { parseString, Builder } = require('xml2js');

const key = fs.readFileSync(path.join(__dirname, 'certs', 'private.key'));
const crt = fs.readFileSync(path.join(__dirname, 'certs', 'public.crt'));

const options = {
  cert: certs.keyCertPairs[0].cert_chain,
  key: certs.keyCertPairs[0].private_key,
  ca: certs.rootCerts,
  json: true
};
const company = {
  name: 'Rc Bike Shop LTDA',
  cnpj: '25906199000142',
  email: 'catalent-sac@roche.com.br',
  ie: '125331380',
  im: '125331382',
  phone: '(31)97513-8751',
  address: { uf: 'mg', city: 'Montes Claros', neighborhood: 'Planalto', street: 'Avenida Bernardo Sayao, 774' },
  csc: '000001',
  cic: '000183',
  certPassword: process.env.PEM_PASSWORD,
  platformPassword: process.env.PLATFORM_PASSWORD,
  cert: { key, crt }
};

const url = `https://localhost:${process.env.HTTP_PORT}`;

describe('Module: Company (MG)', () => {
  before(done => {
    expect(process.env.TOKEN.length).to.be.above(0);
    options.headers = { Authorization: `Bearer ${process.env.TOKEN}`, 'Content-Type': 'application/json' };
    done();
  });
  it('Method: Registry company (JSON)', function(done) {
    options.body = company;
    request.post(`${url}/company`, options, (err, res) => {
      should().not.exist(err);
      should().exist(res.body);
      expect(res.body.insertedCount).to.be.equal(1);
      done();
    });
  });

  it('Method: Get company', done => {
    request.get(`${url}/company/${company.cnpj}`, options, (err, res) => {
      should().not.exist(err);
      should().exist(res.body);
      expect(res.body.cnpj).to.be.equal('25906199000142');
      done();
    });
  });
  it('Method: Update company', done => {
    // CNPJ value cannot be updated.
    company.name = 'ACELOR MITAL';
    company.phone = '(38)99809-8256';
    options.body = company;
    request.put(`${url}/company`, options, (err, res) => {
      should().not.exist(err);
      should().exist(res.body);
      expect(res.body.nModified).to.be.above(0);
      done();
    });
  });
  it.skip('Method: Remove company', done => {
    request.delete(`${url}/company/${company.cnpj}`, options, (err, res) => {
      should().not.exist(err);
      should().exist(res);
      expect(res.body.n).to.be.equal(1);
      done();
    });
  });

  it.skip('Method: Registry company (XML)', done => {
    const builder = new Builder({ headless: true, explicitRoot: false, renderOpts: { pretty: false } });
    const obj = Object.assign({}, company);
    // Attaches certificate as base64.
    obj.cert.crt = Buffer.from(company.cert.crt).toString('base64');
    obj.cert.key = Buffer.from(company.cert.key).toString('base64');
    const xmlCompany = builder.buildObject(obj);
    options.headers['Content-Type'] = 'application/xml';
    options.body = xmlCompany;
    options.json = false;
    request.post(`${url}/company`, options, (err, res) => {
      should().not.exist(err);
      should().exist(res);
      // Getting back the result as object.
      parseString(res.body, (err, obj) => {
        should().not.exist(err);
        expect(parseInt(obj.insertedCount)).to.be.equal(1);
        done();
      });
    });
  });
});
